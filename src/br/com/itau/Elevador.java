package br.com.itau;

import java.util.Scanner;

public class Elevador {
    private int andarAtual;
    private int totalAndares;
    private int capacidade;
    private int totalPessoas;
    private int auxTotalPessoas;

    Scanner scanner = new Scanner(System.in);

    public Elevador(){}

    public Elevador(int andarAtual, int totalAndares, int capacidade, int totalPessoas) {
        this.andarAtual = andarAtual;
        this.totalAndares = totalAndares;
        this.capacidade = capacidade;
        this.totalPessoas = totalPessoas;
    }

    public int getAndarAtual() {
        return andarAtual;
    }

    public void setAndarAtual(int andarAtual) {
        this.andarAtual = andarAtual;
    }

    public int getTotalAndares() {
        return totalAndares;
    }

    public void setTotalAndares(int totalAndares) {
        this.totalAndares = totalAndares;
    }

    public int getCapacidade() {
        return capacidade;
    }

    public void setCapacidade(int capacidade) {
        this.capacidade = capacidade;
    }

    public int getTotalPessoas() {
        return totalPessoas;
    }

    public void setTotalPessoas(int totalPessoas) {
        this.totalPessoas = totalPessoas;
    }

    public void inicializa(){
        System.out.println("Capacidade do elevador: ");
        capacidade = scanner.nextInt();

        System.out.println("Total de andares no prédio: ");
        totalAndares = scanner.nextInt();

        andarAtual = 0;
        totalPessoas = 0;
    }

    public void entra(){
        if(getTotalPessoas() == getCapacidade()){
            System.out.println("Elevador cheio!");
        }else
        {
            totalPessoas++;

        }
        situacao();
    }

    public void sai(){
        if(getTotalPessoas() == 0){
            System.out.println("Elevador vazio!");
        }else{
            totalPessoas--;

        }
        situacao();
    }

    public void sobe(){
        if(getAndarAtual() == getTotalAndares()){
            System.out.println("Último andar!");
        }else{
            andarAtual++;

        }
        situacao();
    }

    public void desce(){
        if(getAndarAtual() == 0){
            System.out.println("Você está no térreo!");
        }else{
            andarAtual--;
            situacao();
        }
    }

    public void situacao(){
        System.out.println("Informações: ");
        System.out.println("Capacidade: " + capacidade + " | "
                         + "Total de andares: " + totalAndares + " | "
                         + "Andar atual: " + andarAtual + " | "
                         + "Total de pessoas: " + totalPessoas);
    }
}
