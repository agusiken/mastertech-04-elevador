package br.com.itau;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int opcao;
        Scanner scanner = new Scanner(System.in);
        Elevador elevador = new Elevador();

        do{
            System.out.println(" ---- ELEVADOR ----");
            System.out.println(" - 1. Inicializa  -");
            System.out.println(" - 2. Entra       -");
            System.out.println(" - 3. Sai         -");
            System.out.println(" - 4. Sobe        -");
            System.out.println(" - 5. Desce       -");
            System.out.println(" - 6. Encerra     -");
            System.out.println("-------------------");
            System.out.print("Opção desejada: ");
            opcao = scanner.nextInt();

            switch(opcao){
                case 1:
                    elevador.inicializa();
                    break;
                case 2:
                    elevador.entra();
                    break;
                case 3:
                    elevador.sai();
                    break;
                case 4:
                    elevador.sobe();
                    break;
                case 5:
                    elevador.desce();
                    break;
                case 6:
                    System.out.println("Encerrando sistema....");
                    break;
                default:
                    System.out.println("Opcao inválida!");
                    break;
            }
        }while(opcao != 6);
    }
}
